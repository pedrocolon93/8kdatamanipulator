﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CameraManager : MonoBehaviour, KinectGestures.GestureListenerInterface
{
    [Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
    public int playerIndex = 0;

    public int maxPlayers = 2;

    private int playerCount = 0;
    // singleton instance of the class

    /// <summary>
    /// Invoked when a new user is detected. Here you can start gesture tracking by invoking KinectManager.DetectGesture()-function.
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    public void UserDetected(long userId, int userIndex)
    {
        // the gestures are allowed for the primary user only
        KinectManager manager = KinectManager.Instance;
        if (!manager)
            return;
        if (playerCount == (maxPlayers))
        {
            return;
        }
        playerCount++;
        GameObject[] draggableGameObjects = GameObject.FindGameObjectWithTag("CameraContainer").transform.GetChild(0).GetComponent<KinectIntegrationManager>().draggableObjects;
        //Initialize the correct amount of cameras
        for (int i = 0; i < playerCount; i++)
        {
            GameObject cameraContainerGameObject = GameObject.FindGameObjectWithTag("CameraContainer");
            GameObject cameraGameObject = cameraContainerGameObject.transform.GetChild(i).gameObject;
            cameraGameObject.SetActive(true);
            Camera camera = cameraGameObject.GetComponent<Camera>();
            cameraGameObject.GetComponent<GestureManager>().UserDetected(userId,userIndex);
            //cameraGameObject.GetComponent<GestureManager>().enabled = false;
            cameraGameObject.GetComponent<KinectIntegrationManager>().draggableObjects = draggableGameObjects;
            float testwidth = 1.0f/playerCount;
            camera.rect = new Rect(i*testwidth,0,testwidth,1);
        }
        manager.refreshGestureListeners();
        foreach (var datasquare in GameObject.FindGameObjectsWithTag("DataSquare"))
        {
            datasquare.GetComponent<BoundBoxes_BoundBox>().enabled = false;
            datasquare.GetComponent<BoundBoxes_BoundBox>().enabled = true;
        }

    }
    

    /// <summary>
    /// Invoked when a user gets lost. All tracked gestures for this user are cleared automatically.
    /// </summary>
    /// <param name="userId">User ID</param>
    /// <param name="userIndex">User index</param>
    public void UserLost(long userId, int userIndex)
    {
        // the gestures are allowed for the primary user only

        //Initialize the correct amount of cameras
        if (userIndex >= maxPlayers)
        {
            return;
        }
        //Deactivate the camera objects
        GameObject cameraContainerGameObject = GameObject.FindGameObjectWithTag("CameraContainer");
        GameObject cameraGameObject = cameraContainerGameObject.transform.GetChild(userIndex).gameObject;
        
        playerCount--;
        if (playerCount == 0)
        {
            playerCount++;
            return;
        }
        cameraGameObject.SetActive(false);
        //Readjust active cameras
        for (int i = 0; i < cameraContainerGameObject.transform.childCount; i++)
        {
            cameraGameObject = cameraContainerGameObject.transform.GetChild(i).gameObject;
            if (cameraGameObject.activeSelf == true)
            {
                Camera camera = cameraGameObject.GetComponent<Camera>();
                float testwidth = 1;
                if (playerCount != 0)
                {
                    testwidth = 1 / playerCount;
                }
                
                camera.rect = new Rect(i * testwidth, 0, testwidth, 1);
            }          
        }
        KinectManager.Instance.refreshGestureListeners();
    }
    
    void Awake()
    {
        
    }

    void Update()
    {
        
    }

    public void GestureInProgress(long userId, int userIndex, KinectGestures.Gestures gesture, float progress, KinectInterop.JointType joint, Vector3 screenPos)
    {
        return;
    }

    public bool GestureCompleted(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint, Vector3 screenPos)
    {
        return false;
    }

    public bool GestureCancelled(long userId, int userIndex, KinectGestures.Gestures gesture, KinectInterop.JointType joint)
    {
        return false;
    }
}
